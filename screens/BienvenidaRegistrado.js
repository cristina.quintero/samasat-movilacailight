import 'react-native-gesture-handler';
import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { Octicons, Ionicons } from '@expo/vector-icons';
import {
  StyledContainer,
  InnerContainer,
  PageLogo,
  LeftIcon,
  StyledInputLabel,
  StyledTextInput,
  RightIcon,
  StyledButtonInicio,
  Colors,
  StyledEspacioInicio,
  PageTitleInicio,
  PageLogoPie,
  Linea,
  ButtonTextInicio,
} from './../components/styles';
import { View, SafeAreaView } from 'react-native';
import KeyboardAvoidingWrapper from '../components/KeyboardAvoidingWrapper';

//Colors
const { brand, darkLight, primary, redSinevin } = Colors;

export default function Login(props) {
  const [hidePassword, setHidePassword] = useState(true);
  const [message, setMessage] = useState();
  const [messageType, setMessageType] = useState();

  //navigation to another page
  const { navigation } = props;
  const goToPage = () => {
    navigation.navigate('Login');
  };

  return (
    <KeyboardAvoidingWrapper>
      <StyledContainer>
        <SafeAreaView />
        <StatusBar style="dark" />
        <InnerContainer>
          <PageLogo resizeMode="cover" source={require('./../assets/img/Acai-Light-Interno-sistema.png')} />
          <StyledEspacioInicio />
          <StyledEspacioInicio />
          <PageTitleInicio>Bienvenido/a : Cristina Quintero</PageTitleInicio>
          <StyledEspacioInicio />

          <StyledButtonInicio>
            <ButtonTextInicio onPress={() => goToPage('Login')}>Ingreso</ButtonTextInicio>
          </StyledButtonInicio>
          <PageLogoPie resizeMode="cover" source={require('./../assets/img/power-by-samasat.png')} />
        </InnerContainer>
      </StyledContainer>
    </KeyboardAvoidingWrapper>
  );
}

const MyTextInput = ({ label, icon, isPassword, hidePassword, setHidePassword, ...props }) => {
  const { navigation } = props;

  return (
    <View>
      <LeftIcon>
        <Octicons name={icon} size={30} color={brand} />
      </LeftIcon>
      <StyledInputLabel>{label}</StyledInputLabel>
      <StyledTextInput {...props} />
      {isPassword && (
        <RightIcon onPress={() => setHidePassword(!hidePassword)}>
          <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'} size={30} color={darkLight} />
        </RightIcon>
      )}
    </View>
  );
};
