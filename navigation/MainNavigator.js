import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import { Octicons, Ionicons } from '@expo/vector-icons';
import Login from '../screens/Login';
import Proveedor from '../screens/Proveedor';
import Registro from '../screens/Registro';
import Inicio from '../screens/Inicio';
import Bienvenida from '../screens/Bienvenida';
import Menu from '../screens/Menu';
import BienvenidaRegistrado from '../screens/BienvenidaRegistrado';
import { Colors } from './../components/styles';
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const sideMenuDisabledScreens = ['Bienvenida', 'Login', 'Registro', 'Inicio'];
const { brand, darkLight, primary, redSinevin } = Colors;
const DrawerNavigator = () => {
  const goToPage = () => {
    navigation.navigate('Login');
  };
  return (
    <Drawer.Navigator screenOptions={{ headerShown: false }} drawerPosition="right">
      <Drawer.Screen
        name="Inicio"
        component={StackNavigator}
        options={({ route }) => {
          const routeName = getFocusedRouteNameFromRoute(route) ?? 'Inicio';
          if (sideMenuDisabledScreens.includes(routeName)) return { swipeEnabled: false };
        }}
      />

      <Drawer.Screen
        name="Configuración"
        component={Menu}
        options={{
          title: 'Configuración',
          drawerIcon: ({ focused, size }) => (
            <Ionicons name="build" size={size} color={focused ? '#d7282f' : '#d7282f'} />
          ),
        }}
      />
      <Drawer.Screen
        name="Cerrar Sesión"
        component={Login}
        options={{
          title: 'Cerrar Sesión',
          drawerIcon: ({ focused, size }) => (
            <Ionicons name="close-circle" size={size} color={focused ? '#d7282f' : '#d7282f'} />
          ),
        }}
      />
      <Drawer.Screen
        name="Politicas de Privacidad"
        component={Menu}
        options={{
          title: 'Politicas de Privacidad',
          drawerIcon: ({ focused, size }) => (
            <Ionicons name="checkbox-outline" size={size} color={focused ? '#d7282f' : '#d7282f'} />
          ),
        }}
      />
      <Drawer.Screen
        name="Terminos y Condiciones"
        component={Menu}
        options={{
          title: 'Terminos y Condiciones',
          drawerIcon: ({ focused, size }) => (
            <Ionicons name="book" size={size} color={focused ? '#d7282f' : '#d7282f'} />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

const StackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Inicio" component={Inicio} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Proveedor" component={Proveedor} />
      <Stack.Screen name="Registro" component={Registro} />
      <Stack.Screen name="Bienvenida" component={Bienvenida} />
      <Stack.Screen name="BienvenidaRegistrado" component={BienvenidaRegistrado} />
      <Stack.Screen name="Menu" component={Menu} />
    </Stack.Navigator>
  );
};

function MainNavigator() {
  return (
    <NavigationContainer>
      <DrawerNavigator />
    </NavigationContainer>
  );
}

export default MainNavigator;
