import 'react-native-gesture-handler';
import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { Formik } from 'formik';
//icons
import { Octicons, Ionicons } from '@expo/vector-icons';
import {
  StyledContainer,
  InnerContainer,
  PageLogo,
  StyledFormArea,
  LeftIcon,
  StyledInputLabel,
  StyledTextInput,
  RightIcon,
  StyledButton,
  ButtonText,
  Colors,
  MsgBox,
  StyledEspacioInicio,
  PageLogoPie,
} from './../components/styles';
import { View, SafeAreaView, ActivityIndicator } from 'react-native';
import KeyboardAvoidingWrapper from '../components/KeyboardAvoidingWrapper';
import axios from 'axios';
//Colors
const { brand, darkLight, primary } = Colors;

export default function OpcionesCliente(props) {
  const [hidePassword, setHidePassword] = useState(true);
  const [message, setMessage] = useState();
  const [messageType, setMessageType] = useState();

  //navigation to another page
  const { navigation } = props;
  const goToPage = () => {
    navigation.navigate('Menu');
  };

  return (
    <KeyboardAvoidingWrapper>
      <StyledContainer>
        <SafeAreaView />
        <StatusBar style="dark" />
        <StyledEspacioInicio />
        <InnerContainer>
          <PageLogo resizeMode="cover" source={require('./../assets/img/Acai-Light-Interno-sistema.png')} />
          <StyledEspacioInicio />
          <Formik>
            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting }) => (
              <StyledFormArea>
                {!isSubmitting && (
                  <StyledButton onPress={handleSubmit}>
                    <ButtonText>Nuevo</ButtonText>
                  </StyledButton>
                )}

                {isSubmitting && (
                  <StyledButton disabled={true}>
                    <ActivityIndicator size="large" color={primary} />
                  </StyledButton>
                )}
              </StyledFormArea>
            )}
          </Formik>
          <StyledEspacioInicio />
          <PageLogoPie resizeMode="cover" source={require('./../assets/img/power-by-samasat.png')} />
          <StyledEspacioInicio />
        </InnerContainer>
      </StyledContainer>
    </KeyboardAvoidingWrapper>
  );
}

const MyTextInput = ({ label, icon, isPassword, hidePassword, setHidePassword, ...props }) => {
  const { navigation } = props;

  return (
    <View>
      <LeftIcon>
        <Octicons name={icon} size={30} color={brand} />
      </LeftIcon>
      <StyledInputLabel>{label}</StyledInputLabel>
      <StyledTextInput {...props} />
      {isPassword && (
        <RightIcon onPress={() => setHidePassword(!hidePassword)}>
          <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'} size={30} color={darkLight} />
        </RightIcon>
      )}
    </View>
  );
};
