import styled from 'styled-components';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';

const StatusBarHeight = Constants.statusBarHeight;

//colors
export const Colors = {
  primary: '#ffffff',
  secondary: '#E5E7EB',
  tertiary: '#1F2937',
  darkLight: '#9CA3AF',
  brand: '#d7282f',
  green: '#10B981',
  red: '#D7282F',
  black1: '#000000',
  greenSinenin: '#37d30b',
  skySinevin: '#727176',
};

const { primary, secondary, tertiary, darkLight, brand, green, red } = Colors;

export const StyledContainer = styled.View`
  flex: 1;
  padding: 25px;
  padding-top: ${StatusBarHeight + 30}px;
  background-color: ${primary};
`;

export const InnerContainer = styled.View`
  flex: 1;
  width: 100%;
  align-items: center;
`;

export const PageLogo = styled.Image`
  width: 250px;
  height: 90px;
`;

export const Menu = styled.Image`
  width: 50px;
  height: 60px;
`;
export const PageLogoView = styled.Image`
  width: 250px;
  height: 90px;
  justify-content: center;
  align-content: center;
  align-items: center;
`;

export const PageLogoPie = styled.Image`
  width: 250px;
  height: 150px;
`;

export const PageTitle = styled.Text`
  font-size: 30px;
  text-align: center;
  font-weight: bold;
  color: ${tertiary};
  padding: 10px;

  ${(props) =>
    props.welcome &&
    `
    font-size: 35px;
    
    `}
`;

export const SubTitle = styled.Text`
  font-size: 18px;
  margin-bottom: 20px;
  letter-spacing: 1px;
  font-weight: bold;
  color: ${tertiary};

  ${(props) =>
    props.welcome &&
    `
        margin-bottom: 5px;
        font-weight: normal;
    
    `}
`;

export const StyledFormArea = styled.View`
  width: 90%;
`;

export const StyledTextInput = styled.TextInput`
  background-color: ${secondary};
  padding: 15px;
  padding-left: 55px;
  padding-right: 55px;
  border-radius: 5px;
  font-size: 16px;
  height: 60px;
  margin-vertical: 3px;
  margin-bottom: 10px;
  color: ${tertiary};
`;

export const StyledInputLabel = styled.Text`
  color: ${tertiary};
  font-size: 13px
  text-align: left;
`;

export const LeftIcon = styled.View`
  left: 15px;
  top: 38px;
  position: absolute;
  z-index: 1;
`;

export const RightIcon = styled.TouchableOpacity`
  right: 15px;
  top: 38px;
  position: absolute;
  z-index: 1;
`;

export const StyledButton = styled.TouchableOpacity`
  padding: 15px;
  background-color: ${brand};
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 10px;
  margin-vertical: 10px;
  height: 55px;
`;

export const ButtonText = styled.Text`
  color: ${primary};
  font-size: 20px;
`;

export const MsgBox = styled.Text`
  text-align: center;
  font-size: 13px;
  color: ${(props) => (props.type == 'SUCCESS' ? green : red)};
`;

// estilos de welcome
export const WelcomeContainer = styled(InnerContainer)`
  padding: 25px;
  padding-top: 10px;
  justify-content: center;
`;

export const Avatar = styled.Image`
    width: 100px;
    height: 100px;
    margin: auto;
    border-radius: 50px;
    border-width: 2px;
    border-color: ${secondary};
    margin-bottom: 10px;
    margin-top; 10px;
`;

export const WelcomeImage = styled.Image`
  height: 200px;
  min-width: 100%;
`;

// estilos de google
export const Line = styled.View`
  height: 1px;
  width: 100%;
  background-color: ${darkLight};
  margin-vertical: 10px;
`;

export const ExtraView = styled.View`
  justify-content: center;
  flex-direction: row;
  align-items: center;
  padding: 10px;
`;

export const ExtraText = styled.Text`
  justify-content: center;
  align-content: center;
  color: ${tertiary};
  font-size: 15px;
`;

export const TextLink = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

export const TextLinkContent = styled.Text`
  color: ${brand};
  font-size: 15px;
`;

// proveedores
export const PageTitleProveedores = styled.Text`
  font-size: 20px;
  text-align: left;
  font-weight: bold;
  color: ${tertiary};
  padding: 10px;

  ${(props) =>
    props.welcome &&
    `
    font-size: 20px;
    
    `}
`;

export const ExtraTextType = styled.Text`
  padding: 10px;
  justify-content: center;
  align-content: center;
  color: ${tertiary};
  font-size: 17px;
`;

export const StyledTextInputType = styled.TextInput`
  background-color: ${secondary};
  padding: 1px;
  padding-left: 10px;
  padding-right: 10px;
  border-radius: 5px;
  font-size: 16px;
  height: 60px;
  margin-vertical: 3px;
  margin-bottom: 10px;
  color: ${tertiary};
`;

export const Linea = styled.View`
  height: 10px;
  width: 100%;
  background-color: #d7282f;
  margin-vertical: 10px;
`;

export const PageTitleInicio = styled.Text`
  font-size: 30px;
  text-align: center;
  font-weight: bold;
  color: ${tertiary};
  padding: 70px;
`;

export const StyledButtonInicio = styled.TouchableOpacity`
  padding: 10px;
  background-color: #d7282f;
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 10px;
  margin-vertical: 10px;
  height: 55px;
  width: 300px;
`;

export const StyledEspacioInicio = styled.TouchableOpacity`
  padding: 35px;
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 5px;
  margin-vertical: 5px;
  height: 60px;
  width: 250px;
`;

export const StyledEspacioBanner = styled.TouchableOpacity`
  padding: 50px;
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 15px;
  margin-vertical: 5px;
  height: 70px;
  width: 20px;
`;

export const ButtonTextInicio = styled.Text`
  color: ${primary};
  font-size: 20px;
`;
export const ButtonTextSiguiente = styled.Text`
  color: #000000;
  font-size: 20px;
`;
export const TextLinkContentInicio = styled.Text`
  color: #d7282f;
  font-size: 20px;
`;
export const StyledInputLabelRegistro = styled.Text`
  color: ${tertiary};
  font-size: 1px;
  text-align: left;
`;

export const StyledTextInputRegistro = styled.TextInput`
  background-color: ${secondary};
  padding: 10px;
  text-align: center;
  border-radius: 5px;
  font-size: 16px;
  height: 55px;
  margin-vertical: 3px;
  margin-bottom: 10px;
  color: ${tertiary};
`;
export const LeftIconSiguiente = styled.View`
  left: 200px;
  top: 15px;
  position: absolute;
  z-index: 10;
`;

export const StyledButtonSiguiente = styled.TouchableOpacity`
  padding: 15px;
  background-color: #ffffff;
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 5px;
  margin-vertical: 5px;
  height: 60px;
  width: 250px;
`;

export const LeftIconMenu = styled.View`
  left: 40px;
  top: 15px;
  position: absolute;
  z-index: 10;
`;

export const StyledButtonMenuIzq = styled.TouchableOpacity`
  left: -100px;
  top: 190px;
  padding: 10px;
  background-color: ${primary};
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 5px;
  margin-vertical: 5px;
  height: 85px;
  width: 135px;
`;

export const StyledButtonMenuDer = styled.TouchableOpacity`
  left: 100px;
  top: -40px;
  padding: 10px;
  background-color: ${primary};
  border-radius: 5px;
  margin-vertical: 5px;
  height: 80px;
  width: 135px;
`;
export const StyledButtonMenuIzqContenedor = styled.TouchableOpacity`
  background-color: ${primary};
  justify-content: flex-end;
  border-radius: 5px;
  margin-vertical: 5px;
`;

export const StyledEspacioIcono = styled.TouchableOpacity`
  height: 20px;
  width: 50px;
`;
export const centro = styled.Text`
  color: #000000;
  font-size: 20px;
`;

export const StyledTextInputRazonSocial = styled.TextInput`
  background-color: ${secondary};
  padding: 10px;
  text-align: center;
  padding-left: 55px;
  padding-right: 55px;
  border-radius: 5px;
  font-size: 16px;
  height: 55px;
  margin-vertical: 3px;
  margin-bottom: 10px;
  color: ${tertiary};
`;
