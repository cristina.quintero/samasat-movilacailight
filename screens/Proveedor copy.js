import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { CheckBox, Icon } from 'react-native-elements';
import { Formik } from 'formik';
import { List } from 'react-native-paper';
import RadioButtonRN from 'radio-buttons-react-native';
import {
  StyledContainer,
  SubTitle,
  Colors,
  StyledTextInputType,
  StyledButton,
  ButtonText,
} from './../components/styles';
+

import { View, SafeAreaView, Picker } from 'react-native';
//keyboard avoiding view
import KeyboardAvoidingWrapper from '../components/KeyboardAvoidingWrapper';
import 'react-native-gesture-handler'; //yo le agregue esto no hace mucho,
import axios from 'axios';

//colors
const { brand, darkLight } = Colors;

const Proveedor = (props) => {
  const [isSelected, setSelection] = useState(false);
  const { navigation } = props;
  const [check1, setCheck1] = useState(false);

  const goToPage = (pageName) => {
    navigation.navigate('Login');
  };
  const data = [
    {
      label: 'Parte Relacionada SI',
    },
    {
      label: 'Parte Relacionada NO',
    },
  ];

  ///servicio
  const handleProveedores = (empresa, idEntidadEmpresa, data) => {
    handleMessage(null);
    const url =
      'http://server3.samasat.com:8888/api/ebe/proveedores/guardar?empresa=' +
      empresa +
      '&idEntidadEmpresa=' +
      idEntidadEmpresa;

    axios
      .post(url, data)
      .then((response) => {
        const result = response.data;
        const { message, status, data } = result;
        const r = response.data.listaUsuario.length;

        if (r == 0) {
          navigation.navigate('Proveedor');
          handleMessage('Credenciales correctas', 'SUCCESS');
          //goToPage("Proveedor");
        } else {
          handleMessage('Credenciales incorrectas', 'FAILED');
        }
        setSubmitting(false);
      })
      .catch((error) => {
        setSubmitting(false);
        handleMessage('Credenciales incorrectas', 'FAILED');
      });
  };
  //API: handle error message
  const handleMessage = (message, type = 'FAILED') => {
    setMessage(message);
    setMessageType(type);
  };
  ///fin servicios
  return (
    <KeyboardAvoidingWrapper>
      <StyledContainer>
        <SafeAreaView />
        <StatusBar style="dark" />

        <SubTitle>DATOS PRINCIPALES</SubTitle>
        <List.Accordion title="Datos Principales">
          <Picker style={{ height: 60, width: 305 }}>
            <Picker.Item label="Seleccione Tipo de Identificación" value="" />
            <Picker.Item label="CÉDULA" value="1" />
            <Picker.Item label="CONSUMIDOR FINAL" value="2" />
            <Picker.Item label="IDENTIFICACION DEL EXTERIOR" value="3" />
            <Picker.Item label="PASAPORTE" value="4" />
            <Picker.Item label="PLACA" value="5" />
            <Picker.Item label="PASAPORTE" value="6" />
            <Picker.Item label="RUC" value="7" />
            <Picker.Item label="SIN IDENTIFICACIÓN" value="8" />
          </Picker>
          <MyTextInput placeholder="Identificación" placeholderTextColor={darkLight} />
          <MyTextInput placeholder="Nombres" placeholderTextColor={darkLight} />
          <MyTextInput placeholder="Apellidos" placeholderTextColor={darkLight} />
          <MyTextInput placeholder="Dirección" placeholderTextColor={darkLight} />
          <MyTextInput placeholder="Correo Electrónico" placeholderTextColor={darkLight} />
          <MyTextInput placeholder="Teléfono" placeholderTextColor={darkLight} />
        </List.Accordion>
        <List.Accordion title="Datos Generales">
          <RadioButtonRN
            data={data}
            selectedBtn={(e) => console.log(e)}
            icon={<Icon name="check-circle" size={25} color="#2c9dd1" />}
          />
          <Picker style={{ height: 60, width: 305 }}>
            <Picker.Item label="Seleccione Origen" value="" />
            <Picker.Item label="EXTRANJERO" value="1" />
            <Picker.Item label="NACIONAL" value="2" />
          </Picker>

          <Picker style={{ height: 60, width: 305 }}>
            <Picker.Item label="Seleccione Tipo de Contribuyente" value="" />
            <Picker.Item label="PERSONA JURIDICA" value="1" />
            <Picker.Item label="PERSONA NATURAL" value="2" />
          </Picker>
          <Picker style={{ height: 60, width: 305 }}>
            <Picker.Item label="Seleccione Tipo Operación Contribuyente" value="" />
            <Picker.Item label="NO OBLIGADO A LLEVAR CONTABILIDAD" value="1" />
            <Picker.Item label="OBLIGADO A LLEVAR CONTABILIDAD" value="2" />
          </Picker>
          <CheckBox left title="Estado" checked={check1} onPress={() => setCheck1(!check1)} />
        </List.Accordion>

        <StyledButton onPress={() => handleProveedores(values)}>
          <ButtonText style>Guardar</ButtonText>
        </StyledButton>
      </StyledContainer>
    </KeyboardAvoidingWrapper>
  );
};

const MyTextInput = ({ label, ...props }) => {
  const { navigation } = props;

  return (
    <View>
      <StyledTextInputType {...props} />
    </View>
  );
};

export default Proveedor;
