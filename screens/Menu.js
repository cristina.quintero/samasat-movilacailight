import 'react-native-gesture-handler';
import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { Octicons, Ionicons } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/FontAwesome5';
import axios from 'axios';
import { FontAwesome } from '@expo/vector-icons';
import {
  StyledContainer,
  InnerContainer,
  PageLogo,
  LeftIcon,
  StyledInputLabel,
  StyledTextInput,
  RightIcon,
  Colors,
  StyledEspacioInicio,
  PageLogoPie,
  StyledButtonMenuIzqContenedor,
  StyledButtonMenuIzq,
  StyledButtonMenuDer,
  ButtonTextSiguiente,
  TextLink,
  TextLinkContentInicio,
  centro,
  ButtonText,
  StyledButton,
} from './../components/styles';
import { View, SafeAreaView, ActivityIndicator, TouchableWithoutFeedback, StyleSheet, Text } from 'react-native';
import KeyboardAvoidingWrapper from '../components/KeyboardAvoidingWrapper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import { connect } from 'formik';
//Colors
const { brand, darkLight, primary, redSinevin } = Colors;

export default function Menu(props) {
  const [hidePassword, setHidePassword] = useState(true);
  const [message, setMessage] = useState();
  const [messageType, setMessageType] = useState();

  const bgStyles = { backgroundColor: '#ffffff', ...styles.bgStyles };
  //navigation to another page
  const { navigation } = props;
  const goToPage = () => {
    navigation.navigate('Login');
  };

  constructor = () => {
    console.log('f'); // prints out whatever is inside props
  };

  const handleLMenu = () => {
    const url =
      'http://server3.samasat.com:8888/api/ebe/seguridad/acceso?user=soporte&password=adminStratos24&sistema=SIS_ACAI_LIGHT-1.0';
    console.log(url);

    axios
      .get(url)
      .then((response) => {
        console.log('aqui1');
        const result = response.data.datosAccesoSeguridad.listaTgenFuncionalidad.length;

        for (let i = 0; i < result; i++) {
          const iconoMenu = response.data.datosAccesoSeguridad.listaTgenFuncionalidad[i].icono;
          console.log(iconoMenu);
        }

        console.log(result);
      })
      .catch((error) => {
        console.log('erro');
      });
  };

  return (
    <KeyboardAvoidingWrapper>
      <StyledContainer>
        <SafeAreaView />
        <StatusBar style="dark" />
        <StyledEspacioInicio />
        <InnerContainer>
          <PageLogo resizeMode="cover" source={require('./../assets/img/Acai-Light-Interno-sistema.png')} />
          <TouchableWithoutFeedback>
            <View style={styles.card}>
              <View style={styles.spacing}>
                <View style={bgStyles}>
                  <Icon name="cart-flatbed" size={30} color={brand} />
                  <FontAwesome name={'book'} size={30} color={brand} />
                  <FontAwesome name={'dollar-sign'} size={30} color={brand} />
                  <FontAwesome name={'basket-shopping'} size={30} color={brand} />
                  <FontAwesome name={'book'} size={30} color={brand} />
                  <Text style={styles.name}>VENTAS</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback>
            <View style={styles.card}>
              <View style={styles.spacing}>
                <View style={bgStyles}>
                  <Icon name="book" size={30} color={brand} />
                  <Text style={styles.name1}>INVENTARIO</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback>
            <View style={styles.cardDerecha}>
              <View style={styles.spacing}>
                <View style={bgStyles}>
                  <Icon name="book" size={30} color={brand} />
                  <Text style={styles.name1}>COMPRAS</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback>
            <View style={styles.cardDerecha}>
              <View style={styles.spacing}>
                <View style={bgStyles}>
                  <Icon name="book" size={30} color={brand} />

                  <Text style={styles.name1}>REPORTES</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <StyledButton onPress={handleLMenu}>
            <ButtonText>ver</ButtonText>
          </StyledButton>
          <PageLogoPie resizeMode="cover" source={require('./../assets/img/power-by-samasat.png')} />
        </InnerContainer>
      </StyledContainer>
    </KeyboardAvoidingWrapper>
  );
}
const LeftContent = (props) => <Avatar.Icon {...props} />;

const MyTextInput = ({ label, icon, isPassword, hidePassword, setHidePassword, ...props }) => {
  const { navigation } = props;

  return (
    <View>
      <LeftIcon>
        <Octicons name={icon} size={30} color={brand} />
      </LeftIcon>
      <StyledInputLabel>{label}</StyledInputLabel>
      <StyledTextInput {...props} />
      {isPassword && (
        <RightIcon onPress={() => setHidePassword(!hidePassword)}>
          <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'} size={30} color={darkLight} />
        </RightIcon>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    flex: 1,
    position: 'relative',
    height: 150,
    width: 140,
    top: 40,
    left: -80,
    borderColor: '#d7282f',
  },

  cardDerecha: {
    flex: 1,
    position: 'relative',
    height: 150,
    width: 140,
    top: -260,
    left: 80,
    borderColor: '#d7282f',
  },
  spacing: {
    flex: 10,
    padding: 5,
  },
  bgStyles: {
    flex: 1,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#d7282f',
    padding: 10,
  },
  number: {
    right: 10,
    top: 10,
    color: '#fff',
    fontSize: 11,
  },
  name: {
    color: '#d7282f',
    fontWeight: 'bold',
    fontSize: 20,
    paddingTop: 10,
    position: 'relative',
    top: 40,
    left: 15,
  },
  name1: {
    color: '#d7282f',
    fontWeight: 'bold',
    fontSize: 20,
    position: 'relative',
    top: 40,
    left: 1,
  },
  image: {
    position: 'absolute',
    bottom: 2,
    right: 2,
    width: 90,
    height: 90,
  },
});
