import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { InnerContainer, PageTitle, SubTitle, StyledFormArea, Line, WelcomeContainer } from '../components/styles';

const Welcome = (props) => {
  const { navigation } = props;

  const goToPage = (pageName) => {
    navigation.navigate(pageName);
  };
  return (
    <>
      <StatusBar style="light" />
      <InnerContainer>
        <WelcomeContainer>
          <PageTitle welcome={true}>BIENVENIDOS</PageTitle>
          <SubTitle welcome={true}>ACAI</SubTitle>
          <StyledFormArea>
            <Line />
          </StyledFormArea>
        </WelcomeContainer>
      </InnerContainer>
    </>
  );
};

export default Welcome;
