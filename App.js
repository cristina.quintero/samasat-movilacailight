import 'react-native-gesture-handler';
import React from 'react';
import { LogBox } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import NavigationDrawer from './navigation/NavigationDrawer';
import { NavigationContainer } from '@react-navigation/native';
import MainStackNavigator from './navigation/MainNavigator';
const Drawer = createDrawerNavigator();
// Ignore log notification by message:
LogBox.ignoreLogs(['Warning: ...']);
import { Colors } from './components/styles';
//colors
const { primary, tertiary } = Colors;
// Ignore all log notifications:
LogBox.ignoreAllLogs();

export default function App() {
  //return <Signup />;

  return <MainStackNavigator />;
}
