import 'react-native-gesture-handler';
import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { Formik } from 'formik';
import {
  StyledContainer,
  InnerContainer,
  PageLogo,
  StyledFormArea,
  StyledButton,
  ButtonText,
  Colors,
  MsgBox,
  PageLogoPie,
  StyledInputLabelRegistro,
  StyledTextInputRegistro,
  StyledEspacioInicio,
  StyledTextInputRazonSocial,
} from '../components/styles';
import { View, SafeAreaView, ActivityIndicator, Image } from 'react-native';
import KeyboardAvoidingWrapper from '../components/KeyboardAvoidingWrapper';
import axios from 'axios';
//Colors
const { darkLight, primary } = Colors;

export default function Login(props) {
  const [hidePassword, setHidePassword] = useState(true);
  const [message, setMessage] = useState();
  const [messageType, setMessageType] = useState();
  const [checked, setChecked] = React.useState(false);
  const [resulRazonSocial, setrazonSocial] = useState();

  //navigation to another page
  const { navigation } = props;
  const goToPage = () => {
    navigation.navigate('Login');
  };

  //servicio2: registrarEmpresa
  const handleLogin = (credentials, setSubmitting) => {
    handleMessage(null);
    const url =
      'http://server3.samasat.com:8888/api/ebe/seguridad/registroEmpresa?sistema=SIS_ACAI-1.1&ruc=' +
      credentials.ruc +
      '&razonSocial=' +
      credentials.razonSocial +
      '&correo=' +
      credentials.correo +
      '&usuario=' +
      credentials.usuario +
      '&password=' +
      credentials.password;
    axios
      .post(url, { credentials })
      .then((response) => {
        const ms = response.data.mensaje;
        if (response.data.status == true) {
          handleMessage(ms, 'SUCCESS');
          console.log(credentials);
          navigation.navigate('Login');
          //goToPage("Proveedor");
        } else {
          handleMessage('Usuario no creado', 'FAILED');
        }
        setSubmitting(false);
      })
      .catch((e) => {
        //console.log(e.data.mensaje);
        setSubmitting(false);
        handleMessage('Usuario no creado', 'FAILED');
      });
  };
  //API: handle error message
  const handleMessage = (message, type = 'FAILED') => {
    setMessage(message);
    setMessageType(type);
  };

  const handlerazonSocial = (resulRazonSocial) => {
    setrazonSocial(resulRazonSocial);
  };

  //servicio3: buscarRUC
  const handleBuscarRUC = (credentials) => {
    handleMessage(null);
    console.log(credentials);
    const url = 'http://174.142.206.137:8005/empresa/detalleRUC/' + credentials;
    console.log(url);
    axios.get(url).then((response) => {
      const ms = response.data.razon_social;
      console.log(ms);
      handleMessage(ms, 'SUCCESS');
      handlerazonSocial(ms);
      resulRazonSocial = ms;
    });
  };
  const onSearch = () => {
    console.log('onSearch');
  };
  return (
    <KeyboardAvoidingWrapper>
      <StyledContainer>
        <SafeAreaView />
        <StatusBar style="dark" />
        <InnerContainer>
          <PageLogo resizeMode="cover" source={require('./../assets/img/Acai-Light-Interno-sistema.png')} />
          <StyledEspacioInicio />
          <Formik
            initialValues={{
              sistema: 'SIS_ACAI_LIGHT-1.0',
              ruc: '',
              razonSocial: '',
              correo: '',
              usuario: '',
              password: '',
            }}
            onSubmit={(values, { setSubmitting }) => {
              //console.log(values);

              if (values.ruc == '') {
                handleMessage('Por favor llene todos los campos');
                setSubmitting(false);
              } else {
                handleLogin(values, setSubmitting);
              }
            }}
          >
            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting }) => (
              <StyledFormArea>
                <MyTextInput
                  placeholder="RUC"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('ruc')}
                  onBlur={handleBlur('ruc')}
                  value={values.ruc}
                />

                <MyTextInput
                  placeholder="Razón Social"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('razonSocial')}
                  onBlur={handleBlur('razonSocial')}
                  value={resulRazonSocial}
                />
                <MyTextInput
                  placeholder="Correo"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('correo')}
                  onBlur={handleBlur('correo')}
                  value={values.correo}
                />
                <MyTextInput
                  placeholder="Usuario"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('usuario')}
                  onBlur={handleBlur('usuario')}
                  value={values.usuario}
                />
                <MyTextInput
                  secureTextEntry={hidePassword}
                  isPassword={true}
                  hidePassword={hidePassword}
                  setHidePassword={setHidePassword}
                  placeholder="Clave"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                />

                <MsgBox type={messageType}>{message}</MsgBox>
                {!isSubmitting && (
                  <StyledButton onPress={handleSubmit}>
                    <ButtonText>Guardar</ButtonText>
                  </StyledButton>
                )}
                {isSubmitting && (
                  <StyledButton disabled={true}>
                    <ActivityIndicator size="large" color={primary} />
                  </StyledButton>
                )}
              </StyledFormArea>
            )}
          </Formik>
          <PageLogoPie resizeMode="cover" source={require('./../assets/img/power-by-samasat.png')} />
        </InnerContainer>
      </StyledContainer>
    </KeyboardAvoidingWrapper>
  );
}

const MyTextInput = ({ label, icon, isPassword, hidePassword, setHidePassword, ...props }) => {
  const { navigation } = props;

  return (
    <View>
      <StyledInputLabelRegistro>{label}</StyledInputLabelRegistro>
      <StyledTextInputRegistro {...props} />
    </View>
  );
};
