import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from '../screens/Login';
import Proveedor from '../screens/Proveedor';
import Registro from '../screens/Registro';
import Inicio from '../screens/Inicio';
import Bienvenida from '../screens/Bienvenida';
import Menu from '../screens/Menu';
import BienvenidaRegistrado from '../screens/BienvenidaRegistrado';

const Drawer = createDrawerNavigator();
import { Colors } from './../components/styles';
//colors
const { primary, tertiary } = Colors;
export default function NavigationDrawer() {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: 'transparent',
        },
        headerTintColor: tertiary,
        headerTransparent: true,
        headerTitle: '',
        headerLeftContainerStyle: {
          paddingLeft: 20,
        },
      }}
      initialRouteName="Inicio"
    >
      <Drawer.Screen name="Inicio" component={Inicio} />
      <Drawer.Screen name="Proveedor" component={Proveedor} />
      <Drawer.Screen name="Registro" component={Registro} />
      <Drawer.Screen name="Bienvenida" component={Bienvenida} />
      <Drawer.Screen name="BienvenidaRegistrado" component={BienvenidaRegistrado} />
      <Drawer.Screen name="Login" component={Login} />
      <Drawer.Screen name="Menu" component={Menu} />
    </Drawer.Navigator>
  );
}
