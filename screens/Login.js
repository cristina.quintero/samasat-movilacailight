import 'react-native-gesture-handler';
import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { Formik } from 'formik';
//icons
import { Octicons, Ionicons } from '@expo/vector-icons';
import {
  StyledContainer,
  InnerContainer,
  PageLogo,
  StyledFormArea,
  LeftIcon,
  StyledInputLabel,
  StyledTextInput,
  RightIcon,
  StyledButton,
  ButtonText,
  Colors,
  MsgBox,
  StyledEspacioInicio,
  PageLogoPie,
} from './../components/styles';
import { View, SafeAreaView, ActivityIndicator } from 'react-native';
import KeyboardAvoidingWrapper from '../components/KeyboardAvoidingWrapper';
import axios from 'axios';
//Colors
const { brand, darkLight, primary } = Colors;

export default function Login(props) {
  const [hidePassword, setHidePassword] = useState(true);
  const [message, setMessage] = useState();
  const [messageType, setMessageType] = useState();

  //navigation to another page
  const { navigation } = props;
  const goToPage = () => {
    navigation.navigate('Menu');
  };

  //API: send request
  const handleLogin = (credentials, setSubmitting) => {
    handleMessage(null);
    const url =
      'http://server3.samasat.com:8888/api/ebe/seguridad/acceso?user=' +
      credentials.user +
      '&password=' +
      credentials.password +
      '&sistema=' +
      credentials.sistema;
    console.log(url);

    axios
      .get(url, { credentials })
      .then((response) => {
        console.log('aqui');
        const result = response.data.codigo;
        const mensaje = response.data.mensaje;

        if (result == 0) {
          navigation.navigate('Menu');
          handleMessage(mensaje, 'SUCCESS');
          //goToPage("Proveedor");
        } else {
          handleMessage(mensaje, 'FAILED');
        }
        setSubmitting(false);
      })
      .catch((error) => {
        setSubmitting(false);
        handleMessage('Credenciales incorrectas', 'FAILED');
      });
  };
  //API: handle error message
  const handleMessage = (message, type = 'FAILED') => {
    setMessage(message);
    setMessageType(type);
  };

  return (
    <KeyboardAvoidingWrapper>
      <StyledContainer>
        <SafeAreaView />
        <StatusBar style="dark" />
        <StyledEspacioInicio />
        <InnerContainer>
          <PageLogo resizeMode="cover" source={require('./../assets/img/Acai-Light-Interno-sistema.png')} />
          <StyledEspacioInicio />
          <Formik
            initialValues={{ user: '', password: '', sistema: 'SIS_ACAI_LIGHT-1.0' }}
            onSubmit={(values, { setSubmitting }) => {
              console.log(values);
              if (values.user == '' || values.password == '') {
                handleMessage('Por favor llene todos los campos');
                setSubmitting(false);
              } else {
                handleLogin(values, setSubmitting);
              }
            }}
          >
            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting }) => (
              <StyledFormArea>
                <MyTextInput
                  icon="person"
                  placeholder="Usuario"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('user')}
                  onBlur={handleBlur('user')}
                  value={values.user}
                />
                <MyTextInput
                  icon="lock"
                  placeholder="Contraseña"
                  placeholderTextColor={darkLight}
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  secureTextEntry={hidePassword}
                  isPassword={true}
                  hidePassword={hidePassword}
                  setHidePassword={setHidePassword}
                />
                <MsgBox type={messageType}>{message}</MsgBox>

                {!isSubmitting && (
                  //<StyledButton onPress={handleSubmit}>
                  <StyledButton onPress={handleSubmit}>
                    <ButtonText>Iniciar Sesión</ButtonText>
                  </StyledButton>
                )}

                {isSubmitting && (
                  <StyledButton disabled={true}>
                    <ActivityIndicator size="large" color={primary} />
                  </StyledButton>
                )}
              </StyledFormArea>
            )}
          </Formik>
          <StyledEspacioInicio />
          <PageLogoPie resizeMode="cover" source={require('./../assets/img/power-by-samasat.png')} />
          <StyledEspacioInicio />
        </InnerContainer>
      </StyledContainer>
    </KeyboardAvoidingWrapper>
  );
}

const MyTextInput = ({ label, icon, isPassword, hidePassword, setHidePassword, ...props }) => {
  const { navigation } = props;

  return (
    <View>
      <LeftIcon>
        <Octicons name={icon} size={30} color={brand} />
      </LeftIcon>
      <StyledInputLabel>{label}</StyledInputLabel>
      <StyledTextInput {...props} />
      {isPassword && (
        <RightIcon onPress={() => setHidePassword(!hidePassword)}>
          <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'} size={30} color={darkLight} />
        </RightIcon>
      )}
    </View>
  );
};
